#ifndef TILE
#define TILE

// a class, which describes a single domino tile
class Tile
{
private:
	int a, b;
public:
	// parameterized constructor
	Tile(int at, int bt); 
	// accessor (getter)
	int GetA(); 
	// accessor (getter)
	int GetB(); 
	// switch the tile values
	void Flip(); 

	// operator overloading. WARNING: this may flip the values of the tile 
	// the operator is non-const, it only works in Domino terms, checks if
	// two tiles are compatible within our game rules
	friend bool operator==(Tile& tHand, Tile& tTable);
};

#endif

