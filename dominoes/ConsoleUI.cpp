#include "ConsoleUI.h"
#include <iostream>
#include "Utils.h"

void MainMenu(){ 
	ConsoleClear();
	std::cout << "+---ABOBA DOMINOES---+" << std::endl;
	std::cout << "1 - Start\n" << "2 - Rules" << std::endl;
	std::cout << "+--------------------+" << std::endl;
}
void DrawTile(Tile tile) {
	int a = tile.GetA();
	int b = tile.GetB();
	std::cout << "[" << a << "|" << b << "]-";
}
void DrawTable(DominoesGame game) {
	ConsoleClear();
	std::cout << "+============TABLE============+" << std::endl;
	int counter = 0;
	for (int i = 0; i < game.table.Size(); i++) {
		counter++;
		DrawTile(game.table.At(i));
		if (counter == 5) {
			std::cout << std::endl;
			counter = 0;
		}
	}
	std::cout << std::endl;
	std::cout << "+=============================+" << std::endl;
}
void IngameMenu(DominoesGame game) {
	std::cout << "Round " << game.GetRoundsCounter() << std::endl;
	std::cout << "-------------" << std::endl;
	std::cout << game.PlayerName << ": " << game.GetPlayerPoints() << " pts." << std::endl;
	std::cout << game.AIName << ": " << game.GetAIPoints() << " pts." << std::endl;
	std::cout << "-------------" << std::endl;

	std::cout << "In market: " << game.market.Size() << " tiles" << std::endl;
	std::cout << game.AIName << "'s hand: (" << game.AIHand.Size() << ")" << std::endl;
	std::cout << "Your hand(" << game.playerHand.Size() << "): " << std::endl;
	for (int i = 0; i < game.playerHand.Size(); i++) {
		DrawTile(game.playerHand.At(i));
	}
	std::cout << std::endl;
	for (int i = 0; i < game.playerHand.Size(); i++) {
		if (i > 9) {
			std::cout << "(" << i + 1 << ") ";
			continue;
		}
		std::cout << " (" << i + 1 << ")  ";
	}
	std::cout << std::endl;
}
void DisplayRules() {
	// TODO: implemet this !!!
	std::cout << "Rules here" << std::endl;
}
void Update(DominoesGame game) {
	DrawTable(game);
	IngameMenu(game);
}

