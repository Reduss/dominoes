#include "DominoesController.h"
#include "ConsoleUI.h"
#include "Utils.h"
#include "DominoesAI.h"
#include <ctime>
#include <iostream>
#include "FileLogger.h"


bool PlayerTurn(DominoesGame& game) {
	while (!CanMakeMove(game, game.playerHand)) {
		// if a player doesnt have the apropriate tile, take from market
		std::cout << "\nNo tile to place, taking some from market...";
		PressAnyKeyToContinue();
		if (game.market.Empty()) {
			// if a player doesn't have a matching tile and the market is empty,
			// skip the turn
			std::cout << "No tiles left in market. Skipping the turn";
			PressAnyKeyToContinue();
			Update(game);
			return false ;
		}
		int n = rand() % game.market.Size();
		game.market.MoveTile(n, game.playerHand);
		Update(game);
	}
	
	bool placed = false;
	while (!placed) { 
		
		// there is a bug with this kind of input: 1           2         3
		// this bug is also present in hte main menu

		std::cout << "\nWhich tile do you want to place?" << std::endl;
		std::string str = "";
		std::cin >> str;
		if (IsNum(str) == false || str.length() > 2) {
			std::cout << "\nIncorrect input.";
			PressAnyKeyToContinue();
			Update(game);
			continue;
		}
		int n = std::stoi(str); // convert string to int
		if (n <= 0 || n > game.playerHand.Size()) {
			// + 1 because user input starts with 1, Size() - with 0
			// check later !!!!
			std::cout << "\nNo such tile";
			PressAnyKeyToContinue();
			Update(game);
			continue;
		}
		if (!Placeable(game.playerHand.At(n - 1), game)) {
			std::cout << "Cant play this tile!";
			PressAnyKeyToContinue();
			Update(game);
			continue;
		}
		int t = n - 1;
		game.playerHand.MoveTile(t, game.table);
		placed = true;
		Update(game);
	}
	return true;
}
bool AITurn(DominoesGame& game) {
	while (!CanMakeMove(game, game.AIHand)) {
		// if the computer doesnt have the apropriate tile, take from market

		// if an AI doesn't have a matching tile and the market is empty,
		// skip the turn
		if (game.market.Empty() && !CanMakeMove(game, game.AIHand)) {
			std::cout << "No tiles left in market." << game.AIName << " skips the turn";
			PressAnyKeyToContinue();
			Update(game);
			return false;
		}
		int n = rand() % game.market.Size();
		game.market.MoveTile(n, game.AIHand);

		
	}
	int n = MakeDecision(game);
	game.AIHand.MoveTile(n, game.table);
	std::cout << std::endl << game.AIName << " has made the move";
	PressAnyKeyToContinue();
	Update(game);
	return true;
}
void StartGame(DominoesGame& game) {
	while (true) {
		MainMenu();
		std::string str = "";
		std::cin >> str;
		if (IsNum(str) == false) {
			std::cout << "Incorrect input.";
			PressAnyKeyToContinue();
			continue;
		}
		int n = std::stoi(str); // convert string to int
		switch (n) {
		case 1:
			StartMatch(game);
			PressAnyKeyToContinue();
			break;
		case 2:
			DisplayRules();
			PressAnyKeyToContinue();
			break;
		default:
			std::cout << "Incorrect input.";
			PressAnyKeyToContinue();
			break;
		}
	}
}
void StartMatch(DominoesGame& game) {
	std::cout << "The match started" << std::endl;	
	while (!game.IsFinished()) {
		game.UpdateSets();
		game.SetDate();
		Round(game);
		game.IsFinished();
	}
	game.SetWinner();
	// log to file
	std::cout << "\nThe game is over. Winner: " << game.winner;
	Log(game);
	std::cout << "\nThe results have been loged into the file" << std::endl;
}
void Round(DominoesGame& game) {
	game.CountRounds();
	bool plflag = true;
	bool aiflag = true;
	while (!game.RoundOver(plflag, aiflag)) {
		// bozhe kak eto ploho blyat'
		Update(game);
		plflag = PlayerTurn(game);
		game.RoundOver(plflag, aiflag);
		aiflag = AITurn(game);
		game.RoundOver(plflag, aiflag);
	}
	// round over cout
	game.CountPoints();
	std::cout << "\nThe round is over!" << std::endl;
	std::cout << "Points: " << game.PlayerName << " - " << game.GetPlayerPoints()
		<< " " << game.AIName << " - " << game.GetAIPoints() << std::endl;
	PressAnyKeyToContinue();
}
bool CanMakeMove(DominoesGame& game, DominoSet& handToCheck) {
	if (game.table.Empty())
		return true;
	for (int i = 0; i < handToCheck.Size(); i++) {
		if (Placeable(handToCheck.At(i), game))
			return true;
	}
	return false;
}
bool Placeable(Tile& t, DominoesGame game) {
	if (game.table.Empty())
		return true;
	if (t == game.table.Last())
		return true;
	return false;
}