#ifndef DOMINO_SET
#define DOMINO_SET

#include <vector>
#include "Tile.h"

// a container class, representing the set of dominoes
class DominoSet
{
private:
	std::vector<Tile> set;		// our container is based on a vector
public:
	// moves the tile from this DominoSet instance to destination
	void MoveTile(int TileIndex, DominoSet& destination);
	// returns the collection
	// DominoSet& GetCollection();
	void Add(Tile t);
	// Remove the tile under ind index
	void Clear();
	// returns the tile at a specified index
	Tile& At(int ind);
	// returns the size of the collection
	int Size();
	// returns the last element of the collection
	Tile& Last();
	// checks if the collections is empty
	bool Empty();
};
#endif
