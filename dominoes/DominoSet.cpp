#include "DominoSet.h"

void DominoSet::MoveTile(int tile, DominoSet& dest) {
	dest.set.push_back(this->set.at(tile));
	this->set.erase(this->set.begin() + tile);
}
void DominoSet::Add(Tile t){
	this->set.push_back(t);
}
void DominoSet::Clear() {
	this->set.clear();
}
Tile& DominoSet::At(int ind)
{
	return this->set.at(ind);
}
int DominoSet::Size() {
	return this->set.size();
}
Tile& DominoSet::Last() {
	return this->set.back();
}
bool DominoSet::Empty() {
	return this->set.empty();
}