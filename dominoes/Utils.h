#ifndef UTILS
#define UTILS

// some utility functions

#include <string>

void PressAnyKeyToContinue();
void ConsoleClear();
bool IsNum(std::string);

#endif