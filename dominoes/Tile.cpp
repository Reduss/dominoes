#include "Tile.h"

Tile::Tile(int at, int bt) {
	this->a = at; // this � a pointer to the current class instance(for which the method is called via "." operator)
	this->b = bt;
}
int Tile::GetA() { return this->a; }
int Tile::GetB() { return this->b; }
void Tile::Flip() {
	int temp = this->a;
	this->a = this->b;
	this->b = temp;
}
bool operator==(Tile& tHand, Tile& tTable) {
	
	if (tHand.a == tTable.b) // we only compare the second value of the tile
		// on the table with the first value in hand.
		return true;
	tHand.Flip(); // flip to see if the value on the other side of the tile 
				  // mathes the one to the table
	if (tHand.a == tTable.b)
		return true;
	else 
		tHand.Flip();
	return false;
}
