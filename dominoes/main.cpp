﻿#include <iostream>
#include <iomanip>
#include <Windows.h>
#include <vector>

#include "DominoesGame.h"
#include "Tile.h"
#include "DominoSet.h"
#include "ConsoleUI.h"
#include "DominoesController.h"

int main()
{
	setlocale(LC_CTYPE, ".1251"); // for Cyrillic in console

	DominoesGame currentGame;

	StartGame(currentGame);
}