#ifndef DOMINOES_CONTROLLER
#define DOMINOES_CONTROLLER

// handles user input and main game loop

#include "DominoesGame.h"

// returns false if a player couldn't make a move
bool PlayerTurn(DominoesGame& game);
// returns false if a computer couldn't make a move
bool AITurn(DominoesGame& game);
// start the game (main menu)
void StartGame(DominoesGame& game);
// start the match (gameplay)
void StartMatch(DominoesGame& game);
void Round(DominoesGame& game);
// checks the conditions to make the turn
bool CanMakeMove(DominoesGame& game, DominoSet& handToCheck);
// checks if a tile can be placed
bool Placeable(Tile& t, DominoesGame game);
#endif 

