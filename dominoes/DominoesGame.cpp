#define _CRT_SECURE_NO_WARNINGS
#include "DominoesGame.h"

void DominoesGame::initialyzeMarket() {
	for (int i = 0; i <= 6; i++) {
		for (int j = i; j <= 6; j++) {
			Tile temp(i, j);
			this->market.Add(temp);
		}
	}
}
void DominoesGame::initialyzeHands() {
	srand(time(NULL));

	for (int i = 0; i < 7; i++) {
		int n = rand() % (28 - i);
		this->market.MoveTile(n, playerHand);
	}
	for (int i = 0; i < 7; i++) {
		int n = rand() % (21 - i);
		this->market.MoveTile(n, AIHand);
	}
}
void DominoesGame::Initialyze() {
	this->initialyzeMarket();
	this->initialyzeHands();

}
void DominoesGame::UpdateSets() {
	// clear the market
	this->market.Clear();
	// clear the table
	this->table.Clear();
	// clear hands
	this->playerHand.Clear();
	this->AIHand.Clear();
	// initialyze new sets
	this->Initialyze();
}
void DominoesGame::MakeMove(int ind, DominoSet hand) {
	hand.MoveTile(ind, table);
}
void DominoesGame::CountPoints() {
	for (int i = 0; i < playerHand.Size(); i++) {
		if (playerHand.At(i).GetA() == 0 && playerHand.At(i).GetB() == 0) {
			playerPoints += 10;
			continue;
		}
		playerPoints += playerHand.At(i).GetA() + playerHand.At(i).GetB();
	}
	for (int i = 0; i < AIHand.Size(); i++) {
		if (AIHand.At(i).GetA() == 0 && AIHand.At(i).GetB() == 0) {
			AIPoints += 10;
			continue;
		}
		AIPoints += AIHand.At(i).GetA() + AIHand.At(i).GetB();
	}
}
int DominoesGame::GetPlayerPoints() {
	return this->playerPoints;
}
int DominoesGame::GetAIPoints() {
	return this->AIPoints;
}
void DominoesGame::SetDate() {
	this->date = std::time(0); // get the current time
}
std::tm* DominoesGame::GetDate() {
	tm* time = localtime(&(this->date));
	return time;
}
void DominoesGame::SetWinner(){
	// !!!!! this has been changed check if works
	if (GetPlayerPoints() >= 50) {
		this->winner = this->AIName;
	}
	else if (GetAIPoints() >= 50) {
		this->winner = this->PlayerName;
	}
	else if (GetAIPoints() >= 50 && GetPlayerPoints() >= 50) {
		std::string drawStr = "Draw";
		this->winner = drawStr;
	}
}
bool DominoesGame::IsFinished() {
	if (this->GetPlayerPoints() >= 50 || this->GetAIPoints() >= 50)
		return true;
	return false;
}
int DominoesGame::GetRoundsCounter() {
	return this->RoundsCounter;
}
void DominoesGame::CountRounds() {
	this->RoundsCounter++;
}
bool DominoesGame::RoundOver(bool pl, bool ai) {
	if (this->AIHand.Empty() || this->playerHand.Empty())
		return true;
	if (ai == false && pl == false)
		return true;
	return false;
}