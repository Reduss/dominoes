#include "FileLogger.h"


void Log(DominoesGame game) {
	std::ofstream of;

	of.open("GameResults.txt", std::ios::app);

	if (of.is_open()) {
		of << "---------------" << std::endl <<
			"Date: " << game.GetDate()->tm_year + 1900 << "-" << 
						game.GetDate()->tm_mon + 1 << "-" <<
						game.GetDate()->tm_mday << "-" <<
						game.GetDate()->tm_hour << ":" <<
						game.GetDate()->tm_min <<
						std::endl <<
			"Rounds: " << game.GetRoundsCounter() << std::endl <<
			"Points: " << std::endl <<
			"   " << game.PlayerName << " - " << game.GetPlayerPoints() << std::endl <<
			"   " << game.AIName << " - " << game.GetAIPoints() << std::endl <<
			"Winner: " << game.winner << std::endl <<
			"---------------" << std::endl;
	}
	of.close();
}
