#ifndef CONSOLE
#define CONSOLE

// for drawing some UI elements

#include "DominoesGame.h"

// draws the main menu 
void MainMenu();
// draws a given tile
void DrawTile(Tile tile);
// Draws the tiles on the table
void DrawTable(DominoesGame game);
// an ingame menu with player's hand and so on
void IngameMenu(DominoesGame game);
// outputs the rules of the game 
void DisplayRules();
// updates the UI
void Update(DominoesGame game);
#endif