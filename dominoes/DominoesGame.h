#ifndef DOMINOES_GAME
#define DOMINOES_GAME

#include "DominoSet.h"
#include <string>
#include <ctime> // used for Date

// a class which describes the logic of the game
class DominoesGame
{
private:
	int playerPoints = 0;
	int AIPoints = 0;
	int RoundsCounter = 0;
	// initialyzes the set of 28 tiles
	void initialyzeMarket();
	// gives each player a set of 7 dominoes
	void initialyzeHands();
public:
	// this fields should be private po horoshemu, sad but true
	time_t date;								// date of the game
	std::string winner;							// winners' name
	const std::string PlayerName = "Player";	// players' name
	const std::string AIName = "KD6-3.7";		// AI's name
	
	DominoSet playerHand;						// players' hand
	DominoSet AIHand;							// computers' hand
	DominoSet table;							// tiles on the table
	DominoSet market;							// market

	// initialyzes the fields
	void Initialyze();
	// clears all sets for a new round
	void UpdateSets();
	// places the tile from the hand to the table
	void MakeMove(int ind, DominoSet hand);
	//counts the points
	void CountPoints();
	// returns players' points
	int GetPlayerPoints();
	//returns AIs' points
	int GetAIPoints();
	void SetDate();
	// return the pointer to a tm struct which represents the date
	std::tm* GetDate();
	//returns the winner of the game
	void SetWinner();
	// checks if one of the players has more than 50 points 
	bool IsFinished();
	// checks the conditions for the round to finish
	bool RoundOver(bool i, bool j);
	int GetRoundsCounter();
	//count the amount of rounds
	void CountRounds();
};
#endif 