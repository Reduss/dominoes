#ifndef AI
#define AI

#include "DominoesGame.h"
#include "DominoSet.h"

// chooses a tile for AI to play
int MakeDecision(DominoesGame g);

#endif;

